
## About Application

The app displays a list of the most popular movies of 2019 obtained from [TMDb API](https://www.themoviedb.org/documentation/api). Basic user scenarios like incorrect data entry, lack of internet is supported. There is also a CustomView for displaying rating of film.
Important: To test the functionality of notifications when the application is closed, you must use a physical device with the developer's mod turned off!

## Technology stack

This application is built using the MVP architecture and library Moxy.
Retrofit2 is used to obtain data, dagger 2 for dependency injection.
The application itself is divided into 3 layers: data, domain and presentation.


