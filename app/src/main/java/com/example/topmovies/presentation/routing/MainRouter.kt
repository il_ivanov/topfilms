package com.example.topmovies.presentation.routing

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import androidx.fragment.app.FragmentTransaction
import com.example.topmovies.R
import com.example.topmovies.presentation.fragments.TopFilmsFragment
import moxy.MvpAppCompatFragment

/**
 *  Router for navigation in [com.example.topmovies.presentation.activities.MainPageActivity]
 *  Supports replacing fragments with adding to the backstack and setting the initial fragment to container
 */
class MainRouter(private val fragmentManager: FragmentManager) : RouterContract {

    override fun replaceFragment(fragment: MvpAppCompatFragment) {
        val uniqTag = fragment::class.java.name
        fragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
            .replace(getContainer(), fragment, uniqTag)
            .addToBackStack(uniqTag)
            .commit()
    }

    override fun setUpInitialFragment(fragment: MvpAppCompatFragment) {
        fragmentManager
            .beginTransaction()
            .replace(getContainer(), fragment)
            .commit()
    }

    override fun getContainer(): Int = R.id.fragmentContainer
    override fun goToInitialFragment() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}