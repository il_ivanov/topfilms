package com.example.topmovies.presentation.fragments

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.topmovies.App
import com.example.topmovies.R
import com.example.topmovies.domain.models.SendNotificationResponse
import com.example.topmovies.domain.models.ValidatorDateResponse
import com.example.topmovies.domain.models.ValidatorTimeResponse
import com.example.topmovies.presentation.presenters.ScheduleViewingPresenter
import com.example.topmovies.presentation.views.ScheduleTimeView
import dagger.Lazy
import kotlinx.android.synthetic.main.fragment_schedule_viewing.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import java.text.DateFormatSymbols
import java.util.*
import javax.inject.Inject


class ScheduleViewingFragment : MvpAppCompatFragment(), ScheduleTimeView {

    private val filmName: String by lazy {
        arguments?.getSerializable(ARGUMENT_FILM_NAME) as String? ?: getString(R.string.unknownFilm)
    }

    private val dateAndTime: Calendar = Calendar.getInstance()

    @Inject
    lateinit var daggerInjectPresenter: Lazy<ScheduleViewingPresenter>

    @InjectPresenter
    lateinit var presenter: ScheduleViewingPresenter

    @ProvidePresenter
    fun providePresenter(): ScheduleViewingPresenter {
        App.appInstance.daggerAppComponent.inject(this)
        return daggerInjectPresenter.get()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_schedule_viewing, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        filmNameTextView.text = getString(R.string.scheduleInformation, filmName)
    }

    private fun initListeners() {
        backArrowImageButton.setOnClickListener {
            activity?.onBackPressed()
        }
        doneButton.setOnClickListener {
            presenter.onButtonDonePressed(filmName)
        }
        chooseDateButton.setOnClickListener {
            val dialog = DatePickerDialog(
                context!!, { datePicker, year, month, day ->
                    presenter.onDateSelected(year, month, day)
                },
                dateAndTime[Calendar.YEAR],
                dateAndTime[Calendar.MONTH],
                dateAndTime[Calendar.DAY_OF_MONTH]
            )
            dialog.datePicker.setMinDate(Calendar.getInstance().getTimeInMillis())
            dialog.show()
        }

        chooseTimeButton.setOnClickListener {
            TimePickerDialog(
                context!!, { timePicker, hour, minute ->
                    presenter.onTimeSelected(hour, minute)
                },
                dateAndTime[Calendar.HOUR_OF_DAY],
                dateAndTime[Calendar.MINUTE], true
            ).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == SuccessDialogFragment.CODE_RESULT_FINISH) {
            (activity as? ScheduleViewingFragment.OnChoosingTimeFinishListener)?.timeChose()
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context !is ScheduleViewingFragment.OnChoosingTimeFinishListener) throw RuntimeException(
            "Activity is trying to attach ScheduleViewingFragment but isn't implementing interface ScheduleViewingFragment.OnChoosingTimeFinishListener"
        )
    }

    interface OnChoosingTimeFinishListener {
        fun timeChose()
    }



    private fun formatMonth(month: Int): String {
        val symbols = DateFormatSymbols(Locale.ENGLISH)
        val monthNames: Array<String> = symbols.getMonths()
        return monthNames[month]
    }

    override fun resultTimeValidation(response: ValidatorTimeResponse) {
        when (response) {
            is ValidatorTimeResponse.ValidateSuccess -> {
                val correctingMinutes: String =
                    if (response.minute < 10) "0${response.minute}" else response.minute.toString()
                chooseTimeButton.text = getString(R.string.timeShowButton, response.hour.toString(), correctingMinutes)
                chooseTimeButton.setBackgroundColor(Color.GREEN)
            }
            else -> {
                chooseDateButton.text = getString(R.string.chooseTime)

                Toast.makeText(
                    context!!,
                    getString(R.string.incorrectTimeResult),
                    Toast.LENGTH_SHORT
                ).show()
                chooseTimeButton.setBackgroundColor(Color.RED)
            }
        }
    }

    override fun resultDateValidation(response: ValidatorDateResponse) {
        when (response) {
            is ValidatorDateResponse.ValidateSuccess -> {
                chooseDateButton.text =
                    getString(R.string.dateShowButton, formatMonth(response.month).toString(),response.day.toString(), response.year.toString())


                chooseDateButton.setBackgroundColor(Color.GREEN)
            }
            else -> {
                chooseDateButton.text = getString(R.string.chooseDate)
                Toast.makeText(
                    context!!,
                    getString(R.string.incorrectDateResult),
                    Toast.LENGTH_SHORT
                ).show()
                chooseDateButton.setBackgroundColor(Color.RED)
            }
        }
    }

    override fun notificationBinded(response: SendNotificationResponse) {
        when (response) {
            SendNotificationResponse.SUCCESS -> {
                navigateToSuccessPage()
            }
            SendNotificationResponse.ERROR_DATE_TOO_OLD -> {
                Toast.makeText(context, getString(R.string.incorrectData), Toast.LENGTH_LONG).show()
            }
            SendNotificationResponse.ERROR_SOME_FIELDS_EMPTY -> {
                Toast.makeText(context, getString(R.string.someFieldsEmptyError), Toast.LENGTH_SHORT).show()
            }
            SendNotificationResponse.ERROR_TIME_EXACTLY -> {
                Toast.makeText(context, getString(R.string.currentTimeError), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(context, getString(R.string.responseBackendError), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun navigateToSuccessPage() {
        fragmentManager?.apply {
            val fragment = SuccessDialogFragment.newInstance()
            fragment.apply {
                setTargetFragment(
                    this@ScheduleViewingFragment,
                    SuccessDialogFragment.CODE_REQUEST_SUCCESS
                )
            }.show(this, null)
        }
    }

    companion object {
        private const val ARGUMENT_FILM_NAME = "FILM_NAME"

        fun newInstance(name: String): ScheduleViewingFragment {
            val fragment = ScheduleViewingFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(ARGUMENT_FILM_NAME, name)
            }
            return fragment
        }
    }
}