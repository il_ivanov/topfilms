package com.example.topmovies.presentation.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import androidx.fragment.app.DialogFragment
import com.example.topmovies.R
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlinx.android.synthetic.main.fragment_dialog_success.*

/**
 * Default success screen for application
 * support auto-close after 1 second and send [CODE_RESULT_FINISH] after it
 */
class SuccessDialogFragment : DialogFragment() {

    private val uiHandler = Handler(Looper.getMainLooper())

    var finishTime = DEFAULT_FINISH_TIME;

    companion object {
        const val CODE_RESULT_FINISH = 1
        const val CODE_REQUEST_SUCCESS = 0
        const val DEFAULT_FINISH_TIME = 1000L;
        fun newInstance() = SuccessDialogFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
        setStyle(DialogFragment.STYLE_NORMAL, R.style.SuccessDialogFragmentTheme)
        uiHandler.postDelayed(
            {
                targetFragment?.onActivityResult(targetRequestCode, CODE_RESULT_FINISH, Intent())
                dismiss()
            },
            DEFAULT_FINISH_TIME
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dialog_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val alphaAnimation = AlphaAnimation(0.2f, 0.8f).apply {
            duration = DEFAULT_FINISH_TIME
        }
        successImageView.startAnimation(alphaAnimation)
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.apply {
            setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }
}