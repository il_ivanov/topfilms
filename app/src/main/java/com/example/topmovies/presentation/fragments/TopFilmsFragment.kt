
package com.example.topmovies.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.topmovies.App
import com.example.topmovies.R
import com.example.topmovies.data.remote.FilmsApi
import com.example.topmovies.domain.models.ConvertedFilm
import com.example.topmovies.presentation.adapters.TopMoviesRecyclerViewAdapter
import com.example.topmovies.presentation.presenters.TopFilmsPresenter
import com.example.topmovies.presentation.views.TopFilmsView
import dagger.Lazy
import kotlinx.android.synthetic.main.fragment_top_films.*
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject


/**
 * Fragment display a list of the best films
 * Parent activity must implement interface [TopFilmsFragment.OnFilmSelectedListener]
 */
class TopFilmsFragment : MvpAppCompatFragment(), TopFilmsView, TopMoviesRecyclerViewAdapter.OnFilmSelectedListener {

    private val filmRecyclerViewAdapter: TopMoviesRecyclerViewAdapter = TopMoviesRecyclerViewAdapter(this)

    @Inject
    lateinit var api: FilmsApi

    @Inject
    lateinit var daggerInjectPresenter: Lazy<TopFilmsPresenter>

    @InjectPresenter
    lateinit var presenter: TopFilmsPresenter

    @ProvidePresenter
    fun providePresenter(): TopFilmsPresenter {
        App.appInstance.daggerAppComponent.inject(this)
        return daggerInjectPresenter.get()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context !is TopFilmsFragment.OnFilmSelectedListener) throw RuntimeException("Activity is trying to attach TopFilmsFragment but isn't implementing interface TopFilmsFragment.OnFilmSelected")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_top_films, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFilmRecyclerView()
        if(savedInstanceState == null) {
            presenter.getListOfFilms()
        }
    }

    private fun initFilmRecyclerView() {
        filmsRecyclerView.apply {
            adapter = filmRecyclerViewAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }

    companion object {
        fun newInstance() =
            TopFilmsFragment()
    }

    override fun onFilmReceived(list: List<ConvertedFilm>) {
        filmRecyclerViewAdapter.populateData(list)
    }

    override fun onErrorWhileLoading() {
        Toast.makeText(context,getString(R.string.downloadingError), Toast.LENGTH_SHORT).show()
    }

    override fun onChooseButtonPressed(name: String) {
        (activity as? OnFilmSelectedListener)?.filmForSchedulingSelected(name)
    }

    interface OnFilmSelectedListener {
        fun filmForSchedulingSelected(name: String)
    }
}