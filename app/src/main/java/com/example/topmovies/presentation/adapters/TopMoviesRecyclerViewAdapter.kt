package com.example.topmovies.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.topmovies.R
import com.example.topmovies.domain.models.ConvertedFilm
import com.example.topmovies.utils.loadImage
import kotlinx.android.synthetic.main.view_holder_film.view.*

class TopMoviesRecyclerViewAdapter(private val onFilmSelectedListener: OnFilmSelectedListener): RecyclerView.Adapter<TopMoviesRecyclerViewAdapter.TopMoviesViewHolder>() {

    private var data: List<ConvertedFilm> = mutableListOf()

    fun populateData(list: List<ConvertedFilm>) {
        data = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopMoviesViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_film,parent,false)
        return TopMoviesViewHolder(v)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: TopMoviesViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class TopMoviesViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            val film = data[position]
            view.apply {
                titleTextView.text = film.title
                dateTextView.text = film.releaseDateConverted
                descriptionTextView.text = film.overview
                posterImageView.loadImage(film.posterPath)
                ratingView.currentRate = film.votePercentage
                bookingTimeButton.setOnClickListener {  onFilmSelectedListener.onChooseButtonPressed(film.title) }
            }
        }
    }
    interface OnFilmSelectedListener {
        fun onChooseButtonPressed(name: String)
    }
}