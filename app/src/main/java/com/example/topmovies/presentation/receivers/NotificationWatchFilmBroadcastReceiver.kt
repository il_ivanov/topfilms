package com.example.topmovies.presentation.receivers

import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.topmovies.App
import com.example.topmovies.R

class NotificationWatchFilmBroadcastReceiver : BroadcastReceiver() {

    companion object {
        const val NOTIFICATION_FILM_NAME = "NOTIFICATION_FILM_NAME"
        const val NAME_UNKNOWN_FILM = "UNKNOWN FILM"
    }

    override fun onReceive(context: Context, intent: Intent) {
        context.let {
            val filmName = intent.getStringExtra(NOTIFICATION_FILM_NAME) ?: NAME_UNKNOWN_FILM
            val notificationManager = NotificationManagerCompat.from(it)
            val notificationBuilder =
                NotificationCompat.Builder(it, App.NOTIFICATION_CHANNEL_REMINDER_WATCH_FILM)
            notificationBuilder
                .setContentTitle(context.getString(R.string.movieTimeNotificationTitle))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentText(context.getString(R.string.timeToWatchMovie, filmName))
                .setSmallIcon(R.drawable.picture_splash_screen)
            val notification: Notification = notificationBuilder.build()
            notificationManager.notify(intent.data.hashCode(), notification)
        }
    }
}