package com.example.topmovies.presentation.presenters

import com.example.topmovies.domain.interactors.TopFilmsInteractor
import com.example.topmovies.presentation.views.TopFilmsView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class TopFilmsPresenter @Inject constructor(private val interactor: TopFilmsInteractor) :
    MvpPresenter<TopFilmsView>() {

    private val compositeDisposable = CompositeDisposable()

    fun getListOfFilms() {
        compositeDisposable.add(interactor.receiveFilms()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                viewState.onFilmReceived(it)
            }, {
                viewState.onErrorWhileLoading()
            })
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

}