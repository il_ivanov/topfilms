package com.example.topmovies.presentation.customviews

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.example.topmovies.R
import com.example.topmovies.utils.dpToPx
import kotlin.math.roundToInt


class CustomRateView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : View(context, attrs, defStyle) {

    var currentRate = 0
        set(value) {
            field = value
            invalidate()
        }
    private var minWidth = dpToPx(MIN_WIDTH_DP).roundToInt()
    private var minHeight = dpToPx(MIN_HEIGHT_DP).roundToInt()
    private var textSizePx = dpToPx(MIN_TEXT_SP).roundToInt()
    private var paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var maxRate = 100
    private var minRate = 0

    companion object {
        private const val ALPHA_BACKGROUND_CIRCLE = 170
        private const val COEFFICIENT_PROGRESS_OVAL__RADIUS_OF_RADIUS_CIRCLE = 0.85f
        private const val COEFFICIENT_PROGRESS_OVAL_STROKE_OF_RADIUS_CIRCLE = 0.15f
        private const val START_ANGLE_PROGRESS_ARC = 270f
        private const val ONE_PERCENT_OF_CIRCLE = 3.6f
        private const val MIN_WIDTH_DP = 10
        private const val MIN_TEXT_SP = 15
        private const val MIN_HEIGHT_DP = 10
    }

    init {
        attrs?.let {
            val typedArray =
                context.theme.obtainStyledAttributes(it, R.styleable.CustomRateView, 0, 0)
            typedArray.let {
                minRate = it.getInteger(R.styleable.CustomRateView_minRate, 0)
                maxRate = it.getInteger(R.styleable.CustomRateView_maxRate, 100)
                currentRate = it.getInteger(R.styleable.CustomRateView_currentRate, 0)
            }
            typedArray.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = minWidth + paddingLeft + paddingRight
        val desiredHeight = minHeight + paddingTop + paddingBottom
        val measuredWidth = resolveSizeAndState(desiredWidth, widthMeasureSpec, 0)
        val measuredHeight = resolveSizeAndState(desiredHeight, heightMeasureSpec, 0)
        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    override fun onDraw(canvas: Canvas?) {

        val cy: Int = paddingTop + (height - paddingBottom - paddingTop) / 2
        val cx: Int = paddingLeft + (width - paddingRight - paddingLeft) / 2
        val radius =
            Math.min(
                (height - paddingBottom - paddingTop) / 2,
                (width - paddingRight - paddingLeft) / 2
            )

        paint.apply {
            color = Color.BLACK
            alpha = ALPHA_BACKGROUND_CIRCLE
        }
        canvas?.drawCircle(cx.toFloat(), cy.toFloat(), radius.toFloat(), paint)

        // Progress line
        val radiusProgressOval: Float = radius * COEFFICIENT_PROGRESS_OVAL__RADIUS_OF_RADIUS_CIRCLE

        paint.apply {
            style = Paint.Style.STROKE;
            strokeWidth = radius * COEFFICIENT_PROGRESS_OVAL_STROKE_OF_RADIUS_CIRCLE;
        }

        val onePercentOfRate = (maxRate / 100)
        val progressRateColorPercent = currentRate / onePercentOfRate

        paint.color = when (progressRateColorPercent) {
            in 0..30 -> Color.RED
            in 31..70 -> Color.YELLOW
            else -> Color.GREEN
        }
        val progressRateAngle = currentRate / onePercentOfRate * ONE_PERCENT_OF_CIRCLE
        canvas?.drawArc(
            cx - radiusProgressOval,
            cy - radiusProgressOval,
            cx + radiusProgressOval,
            cy + radiusProgressOval,
            START_ANGLE_PROGRESS_ARC,
            progressRateAngle.toFloat(),
            false,
            paint
        );

        // Rating text
        paint.apply {
            color = Color.WHITE
            strokeWidth = 1f;
            textAlign = Paint.Align.CENTER;
            style = Paint.Style.FILL_AND_STROKE;
            textSize = textSizePx.toFloat()
        }

        val xPos = width / 2.toFloat()
        // yPos provide coordinate for vertical align CENTER
        val yPos =
            (height / 2 - (paint.descent() + paint.ascent()) / 2).toFloat()
        canvas?.drawText(currentRate.toString(), xPos, yPos, paint)
    }
}