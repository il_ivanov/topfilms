package com.example.topmovies.presentation.views

import com.example.topmovies.domain.models.SendNotificationResponse
import com.example.topmovies.domain.models.ValidatorDateResponse
import com.example.topmovies.domain.models.ValidatorTimeResponse
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ScheduleTimeView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun resultTimeValidation(response : ValidatorTimeResponse)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun resultDateValidation(response : ValidatorDateResponse)

    @StateStrategyType(SkipStrategy::class)
    fun notificationBinded(response: SendNotificationResponse)
}