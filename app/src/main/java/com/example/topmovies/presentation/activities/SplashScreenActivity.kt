package com.example.topmovies.presentation.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AlphaAnimation
import com.example.topmovies.R
import com.example.topmovies.utils.hasInternetConnection
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : BaseActivity() {

    override fun getChildLayout(): Int = R.layout.activity_splash_screen

    companion object {
        private const val DELAY_BEFORE_ROUTING: Long = 2000
    }

    private val uiHandler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logoAlphaAnimation()
        navigateTo(MainPageActivity::class.java)
    }

    private fun logoAlphaAnimation() {
        val alphaAnimation = AlphaAnimation(0.2f, 1.0f).apply {
            duration = 1500
        }
        logoCompanyImageView.startAnimation(alphaAnimation)
    }

    private fun navigateTo(jclass: Class<out BaseActivity>) {
        uiHandler.postDelayed(
            {
                if (!this.isDestroyed) {
                    val intent =
                    if(hasInternetConnection(this)) {
                        Intent(this, jclass)
                    } else {
                        Intent(this, NoInternetConnectionActivity::class.java)
                    }
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    finish()
                }
            },
            DELAY_BEFORE_ROUTING
        )
    }

}