package com.example.topmovies.presentation.activities

import android.os.Bundle
import com.example.topmovies.R
import com.example.topmovies.presentation.fragments.ScheduleViewingFragment
import com.example.topmovies.presentation.fragments.TopFilmsFragment
import com.example.topmovies.presentation.routing.MainRouter

class MainPageActivity : BaseActivity(), TopFilmsFragment.OnFilmSelectedListener, ScheduleViewingFragment.OnChoosingTimeFinishListener {

    private lateinit var router: MainRouter

    override fun getChildLayout(): Int = R.layout.activity_main_page

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        router = MainRouter(supportFragmentManager)
        if (savedInstanceState == null) {
            router.setUpInitialFragment(TopFilmsFragment.newInstance())
        }
    }

    override fun filmForSchedulingSelected(name: String) {
        router.replaceFragment(ScheduleViewingFragment.newInstance(name))
    }

    override fun timeChose() {
        router.goToInitialFragment()
    }
}