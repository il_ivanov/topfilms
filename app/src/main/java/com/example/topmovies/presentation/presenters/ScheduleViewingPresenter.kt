package com.example.topmovies.presentation.presenters

import com.example.topmovies.domain.interactors.ScheduleInteractor
import com.example.topmovies.presentation.views.ScheduleTimeView
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class ScheduleViewingPresenter @Inject constructor(private val interactor: ScheduleInteractor) :
    MvpPresenter<ScheduleTimeView>() {

    fun onTimeSelected(hour: Int, minute: Int) {
        viewState.resultTimeValidation(interactor.validateTime(hour, minute))
    }

    fun onDateSelected(year: Int, month: Int, day: Int) {
        viewState.resultDateValidation(interactor.validateDate(year, month, day))
    }

    fun onButtonDonePressed(filmName: String) {
        viewState.notificationBinded(interactor.scheduleFilm(filmName))
    }
}