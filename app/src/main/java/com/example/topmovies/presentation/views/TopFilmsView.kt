package com.example.topmovies.presentation.views

import com.example.topmovies.domain.models.ConvertedFilm
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface TopFilmsView : MvpView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun onFilmReceived(list : List<ConvertedFilm>)

    @StateStrategyType(SkipStrategy::class)
    fun onErrorWhileLoading()
}