package com.example.topmovies.presentation.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import moxy.MvpAppCompatActivity

/**
 * Base class of activity:
 *      Child classes must override the getChildLayout method and return the root layout for inflate.
 */
abstract class BaseActivity : MvpAppCompatActivity() {

    abstract fun getChildLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getChildLayout())
    }
}