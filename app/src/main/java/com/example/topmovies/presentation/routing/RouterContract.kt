package com.example.topmovies.presentation.routing

import androidx.fragment.app.Fragment
import moxy.MvpAppCompatFragment

interface RouterContract {
    fun replaceFragment(fragment: MvpAppCompatFragment)
    fun setUpInitialFragment(fragment: MvpAppCompatFragment)
    fun getContainer(): Int
    fun goToInitialFragment()
}