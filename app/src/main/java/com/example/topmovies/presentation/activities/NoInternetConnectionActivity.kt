package com.example.topmovies.presentation.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.topmovies.R
import com.example.topmovies.utils.hasInternetConnection
import kotlinx.android.synthetic.main.activity_no_internet_connection.*

class NoInternetConnectionActivity : BaseActivity() {
    override fun getChildLayout(): Int = R.layout.activity_no_internet_connection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        noInternetConnectionTextView.setOnClickListener {
            if (hasInternetConnection(this)) {
                startActivity(Intent(this, MainPageActivity::class.java))
                overridePendingTransition(0,0)
                finish()
            } else {
                Toast.makeText(this, getString(R.string.internetStatusNegative),Toast.LENGTH_SHORT).show()
            }
        }
    }

}