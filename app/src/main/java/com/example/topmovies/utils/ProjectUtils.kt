package com.example.topmovies.utils

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide


fun hasInternetConnection(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connectivityManager.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

fun View.dpToPx(dp: Int): Float {
    return dp * context.resources.displayMetrics.density
}

fun ImageView.loadImage(stringUrl: String) {
    Glide.with(this.context).load(stringUrl).centerCrop().into(this);
}
