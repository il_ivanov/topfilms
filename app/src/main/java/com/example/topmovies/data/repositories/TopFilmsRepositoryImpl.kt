package com.example.topmovies.data.repositories

import com.example.topmovies.data.models.Film
import com.example.topmovies.data.models.FilmApiResponse
import com.example.topmovies.data.remote.FilmsApi
import com.example.topmovies.domain.datacontract.TopFilmsRepository
import io.reactivex.Single
import javax.inject.Inject

class TopFilmsRepositoryImpl @Inject constructor(private val filmsApi: FilmsApi) : TopFilmsRepository {

    override fun fetchFilmsRemote(): Single<FilmApiResponse> {
        return filmsApi.getFimResponse()
    }
}