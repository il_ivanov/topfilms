package com.example.topmovies.data.remote

import com.example.topmovies.data.models.Film
import com.example.topmovies.data.models.FilmApiResponse
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface FilmsApi {

    @Headers(
        "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxY2RmODUwYzgwZTM1ZjE5Nzg2ODMxNGY0MjA0YzdiYyIsInN1YiI6IjVmMjE4ZDRhNWIyZjQ3MDAzNjRmODhhZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.IdtvWRtFqCgGxmoJ2bgAipIjSFAWLOqbXvqQthUhrm4",
        "Content-Type: application/json;charset=utf-8"
    )
    @GET("/3/discover/movie?sort_by=popularity.desc&include_adult=false&include_video=false&primary_release_year=2019")
    fun getFimResponse() : Single<FilmApiResponse>
}