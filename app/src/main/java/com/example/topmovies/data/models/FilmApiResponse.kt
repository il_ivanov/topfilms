package com.example.topmovies.data.models

import com.google.gson.annotations.SerializedName

data class FilmApiResponse(@SerializedName("results") val results : List<Film>)