package com.example.topmovies

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.example.topmovies.di.AppComponent
import com.example.topmovies.di.DaggerAppComponent

class App : Application() {

    companion object {
        lateinit var appInstance: App
        const val NOTIFICATION_CHANNEL_REMINDER_WATCH_FILM =
            "NOTIFICATION_CHANNEL_REMINDER_WATCH_FILM"
    }

    lateinit var daggerAppComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        initNotificationChannels()
        daggerAppComponent = DaggerAppComponent
            .builder()
            .build()
    }

    private fun initNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val watchFilmNotificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_REMINDER_WATCH_FILM, getString(
                    R.string.notificationReminderFilmName
                ), NotificationManager.IMPORTANCE_LOW
            )
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannels(listOf(watchFilmNotificationChannel))
        }
    }
}