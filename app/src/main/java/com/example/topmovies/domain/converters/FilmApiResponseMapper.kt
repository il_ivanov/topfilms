package com.example.topmovies.domain.converters

import com.example.topmovies.data.models.FilmApiResponse
import com.example.topmovies.domain.models.ConvertedFilm
import java.text.DateFormat
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class FilmApiResponseMapper {

    fun apiResponseToUiMapping(response: FilmApiResponse): List<ConvertedFilm> {
        val convertedList = mutableListOf<ConvertedFilm>()
        response.results.forEach {
            convertedList.add(
                ConvertedFilm(
                    it.id,
                    "https://image.tmdb.org/t/p/w500/${it.posterPath}",
                    it.backdropPath,
                    it.originalTitle,
                    it.title,
                    convertAverage(it.voteAverage),
                    it.overview,
                    convertDate(it.releaseDate)
            )
            )
        }
        return convertedList.toList()
    }

    private fun convertDate(releaseDate: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val calendar = Calendar.getInstance()
        calendar.time = df.parse(releaseDate) ?: return releaseDate
        val finalDate = "${formatMonth(calendar.get(Calendar.MONTH))} ${calendar.get(Calendar.DAY_OF_MONTH)}, ${calendar.get(Calendar.YEAR)}"
        return finalDate;
    }
    private fun convertAverage(average: Double): Int {
        return (average * 10).roundToInt()
    }

    private fun formatMonth(month: Int): String {
        val symbols = DateFormatSymbols(Locale.ENGLISH)
        val monthNames: Array<String> = symbols.getMonths()
        return monthNames[month]
    }
}
