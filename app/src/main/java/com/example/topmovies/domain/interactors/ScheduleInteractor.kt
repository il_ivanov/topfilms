package com.example.topmovies.domain.interactors


import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.net.Uri
import android.os.Build
import com.example.topmovies.App
import com.example.topmovies.domain.models.NotificationExpectedTime
import com.example.topmovies.domain.models.SendNotificationResponse
import com.example.topmovies.domain.models.ValidatorDateResponse
import com.example.topmovies.domain.models.ValidatorTimeResponse
import com.example.topmovies.domain.validators.DateAndTimeValidator
import com.example.topmovies.presentation.receivers.NotificationWatchFilmBroadcastReceiver
import java.util.*
import javax.inject.Inject

class ScheduleInteractor @Inject constructor(private val validator: DateAndTimeValidator) {

    private val notificationTime = NotificationExpectedTime()

    fun validateDate(year: Int?, month: Int?, day: Int?): ValidatorDateResponse {
        val response = validator.isDateCorrect(year, month, day)
        when (response) {
            is ValidatorDateResponse.ValidateDateEmpty -> {
            }
            is ValidatorDateResponse.ValidateSuccess -> {
                notificationTime.year = year
                notificationTime.month = month
                notificationTime.day = day
            }
        }
        return response
    }

    fun validateTime(hour: Int?, minute: Int?): ValidatorTimeResponse {
        val response = validator.isTimeCorrect(hour, minute)
        when (response) {
            is ValidatorTimeResponse.ValidateEmpty -> {
            }
            is ValidatorTimeResponse.ValidateSuccess -> {
                notificationTime.hour = hour
                notificationTime.minutes = minute
            }
        }
        return response
    }

    fun scheduleFilm(filmName: String): SendNotificationResponse {
        val response = validator.requestToBookingTime(notificationTime)
        if (response == SendNotificationResponse.SUCCESS) {
            organizeNotification(filmName)
        }
        return response
    }

    private fun organizeNotification(filmName: String) {
        val intent = Intent(
            App.appInstance.applicationContext, NotificationWatchFilmBroadcastReceiver::class.java
        )
        intent.putExtra(NotificationWatchFilmBroadcastReceiver.NOTIFICATION_FILM_NAME, filmName)
        val data: Uri = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))
        intent.data = data
        val pendingIntent = PendingIntent.getBroadcast(
            App.appInstance.applicationContext,
            0,
            intent,
            0
        )
        val alarmManager =
            App.appInstance.applicationContext.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
        val calendar: Calendar = Calendar.getInstance().apply {
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0);
            set(Calendar.YEAR, notificationTime.year ?: get(Calendar.YEAR))
            set(Calendar.MONTH, notificationTime.month ?: get(Calendar.MONTH))
            set(Calendar.DAY_OF_MONTH, notificationTime.day ?: get(Calendar.DAY_OF_MONTH))
            set(Calendar.MINUTE, notificationTime.minutes ?: get(Calendar.MINUTE))
            set(Calendar.HOUR_OF_DAY, notificationTime.hour ?: get(Calendar.HOUR_OF_DAY))
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                pendingIntent
            );
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent);
        }

    }
}
