package com.example.topmovies.domain.interactors

import com.example.topmovies.data.models.Film
import com.example.topmovies.domain.converters.FilmApiResponseMapper
import com.example.topmovies.domain.datacontract.TopFilmsRepository
import com.example.topmovies.domain.models.ConvertedFilm
import io.reactivex.Single
import javax.inject.Inject

class TopFilmsInteractor @Inject constructor(private val repository: TopFilmsRepository, private val mapper: FilmApiResponseMapper) {

    fun receiveFilms(): Single<List<ConvertedFilm>> {
        return repository.fetchFilmsRemote()
            .map(mapper::apiResponseToUiMapping)
    }
}