package com.example.topmovies.domain.models

data class NotificationExpectedTime(
    var year: Int? = null,
    var month: Int? = null,
    var day: Int? = null,
    var hour: Int? = null,
    var minutes: Int? = null
)