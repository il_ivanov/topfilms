package com.example.topmovies.domain.models

data class ConvertedFilm(
    val id: Int,
    val posterPath: String,
    val backdropPath: String,
    val originalTitle: String,
    val title: String,
    val votePercentage: Int,
    val overview: String,
    val releaseDateConverted: String
)
