package com.example.topmovies.domain.models

sealed class ValidatorTimeResponse {

    data class ValidateSuccess(val hour: Int, val minute: Int): ValidatorTimeResponse()
    object ValidateEmpty : ValidatorTimeResponse()
}
