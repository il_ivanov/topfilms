package com.example.topmovies.domain.models


sealed class ValidatorDateResponse {

    data class ValidateSuccess(
        val year: Int,
        val month: Int,
        val day: Int
    ) : ValidatorDateResponse()
    object ValidateDateEmpty : ValidatorDateResponse()

}