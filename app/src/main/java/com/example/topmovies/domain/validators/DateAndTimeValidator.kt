package com.example.topmovies.domain.validators

import com.example.topmovies.domain.models.NotificationExpectedTime
import com.example.topmovies.domain.models.SendNotificationResponse
import com.example.topmovies.domain.models.ValidatorDateResponse
import com.example.topmovies.domain.models.ValidatorTimeResponse
import java.util.*

class DateAndTimeValidator {

    fun isTimeCorrect(hour: Int?, minute: Int?): ValidatorTimeResponse {
        return if (hour == null || minute == null) {
            ValidatorTimeResponse.ValidateEmpty
        } else {
            ValidatorTimeResponse.ValidateSuccess(hour, minute)
        }
    }

    fun isDateCorrect(year: Int?, month: Int?, day: Int?): ValidatorDateResponse {
        return if (year == null || month == null || day == null) {
            ValidatorDateResponse.ValidateDateEmpty
        } else {
            ValidatorDateResponse.ValidateSuccess(year, month, day)
        }
    }

    fun requestToBookingTime(notificationTime: NotificationExpectedTime): SendNotificationResponse {
        if (isDateCorrect(
                notificationTime.year,
                notificationTime.month,
                notificationTime.day
            ) !is ValidatorDateResponse.ValidateSuccess
        ) {
            return SendNotificationResponse.ERROR_SOME_FIELDS_EMPTY
        }
        if (isTimeCorrect(
                notificationTime.hour,
                notificationTime.minutes
            ) !is ValidatorTimeResponse.ValidateSuccess
        ) {
            return SendNotificationResponse.ERROR_SOME_FIELDS_EMPTY
        }
        return isChoseTimeGreaterThanCurrect(notificationTime)
    }

    private fun isChoseTimeGreaterThanCurrect(notificationTime: NotificationExpectedTime): SendNotificationResponse {
        val hour = notificationTime.hour ?: return SendNotificationResponse.ERROR_OTHER
        val minute = notificationTime.minutes ?: return SendNotificationResponse.ERROR_OTHER
        val calendar = Calendar.getInstance()
        if (notificationTime.day == calendar.get(Calendar.DAY_OF_MONTH) &&
            notificationTime.month == calendar.get(Calendar.MONTH) &&
            notificationTime.year == calendar.get(Calendar.YEAR)
        ) {
            if (hour < calendar.get(Calendar.HOUR_OF_DAY) || ((notificationTime.hour == calendar.get(
                    Calendar.HOUR_OF_DAY
                ) && minute <= calendar.get(Calendar.MINUTE)))
            ) {
                if (minute == calendar.get(Calendar.MINUTE)) {
                    return SendNotificationResponse.ERROR_TIME_EXACTLY
                }
                return SendNotificationResponse.ERROR_DATE_TOO_OLD
            }
        }
        return SendNotificationResponse.SUCCESS
    }

}