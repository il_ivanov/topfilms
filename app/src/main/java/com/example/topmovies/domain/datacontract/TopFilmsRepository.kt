package com.example.topmovies.domain.datacontract

import com.example.topmovies.data.models.FilmApiResponse
import io.reactivex.Single

interface TopFilmsRepository {
    fun fetchFilmsRemote(): Single<FilmApiResponse>
}