package com.example.topmovies.domain.models

enum class SendNotificationResponse {
    SUCCESS, ERROR_DATE_TOO_OLD, ERROR_OTHER, ERROR_SOME_FIELDS_EMPTY, ERROR_TIME_EXACTLY
}