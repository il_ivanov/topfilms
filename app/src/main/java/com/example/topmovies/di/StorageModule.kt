package com.example.topmovies.di

import com.example.topmovies.data.remote.FilmsApi
import com.example.topmovies.data.repositories.TopFilmsRepositoryImpl
import com.example.topmovies.domain.datacontract.TopFilmsRepository
import dagger.Module
import dagger.Provides

@Module
class StorageModule {

    @Provides
    fun provideTopFilmsRepository(remoteApi: FilmsApi): TopFilmsRepository = TopFilmsRepositoryImpl(remoteApi)
}