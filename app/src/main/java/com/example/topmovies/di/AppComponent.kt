package com.example.topmovies.di

import com.example.topmovies.presentation.fragments.ScheduleViewingFragment
import com.example.topmovies.presentation.fragments.TopFilmsFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
    modules = arrayOf(
        CoreModule::class,
        StorageModule::class,
        UsecaseModule::class,
        NetworkModule::class
    )
)
interface AppComponent {

    fun inject(fragment: TopFilmsFragment)
    fun inject(fragment: ScheduleViewingFragment)
}