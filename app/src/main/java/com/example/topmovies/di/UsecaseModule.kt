package com.example.topmovies.di

import com.example.topmovies.data.repositories.TopFilmsRepositoryImpl
import com.example.topmovies.domain.converters.FilmApiResponseMapper
import com.example.topmovies.domain.datacontract.TopFilmsRepository
import com.example.topmovies.domain.interactors.ScheduleInteractor
import com.example.topmovies.domain.interactors.TopFilmsInteractor
import com.example.topmovies.domain.validators.DateAndTimeValidator
import dagger.Module
import dagger.Provides

@Module
class UsecaseModule {

    @Provides
    fun provideTopFilmsInteractor(
        repository: TopFilmsRepository,
        mapper: FilmApiResponseMapper
    ): TopFilmsInteractor = TopFilmsInteractor(repository, mapper)

    @Provides
    fun provideFilmApiResponseMapper() = FilmApiResponseMapper()

    @Provides
    fun provideSchedulingInteractor(validator: DateAndTimeValidator) = ScheduleInteractor(validator)

    @Provides
    fun provideValidator() = DateAndTimeValidator()
}