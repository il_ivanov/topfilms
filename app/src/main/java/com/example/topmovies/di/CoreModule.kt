package com.example.topmovies.di

import com.example.topmovies.domain.interactors.ScheduleInteractor
import com.example.topmovies.domain.interactors.TopFilmsInteractor
import com.example.topmovies.presentation.presenters.ScheduleViewingPresenter
import com.example.topmovies.presentation.presenters.TopFilmsPresenter
import dagger.Module
import dagger.Provides

@Module
class CoreModule {

        @Provides
        fun provideTopFilmPresenter(interactor: TopFilmsInteractor): TopFilmsPresenter = TopFilmsPresenter(interactor)

        @Provides
        fun provideScheduleViewingPresenter(interactor: ScheduleInteractor): ScheduleViewingPresenter = ScheduleViewingPresenter(interactor)
}